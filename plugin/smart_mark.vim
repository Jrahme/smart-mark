"
"        Smart Mark - A vim plugin to display mark locations in the gutter
"        Copyright (C) 2018  Jacob Rahme <jacob@jrahme.ca>
"
"        This program is free software: you can redistribute it and/or modify
"        it under the terms of the GNU General Public License as published by
"        the Free Software Foundation, either version 3 of the License, or
"        (at your option) any later version.
"
"        This program is distributed in the hope that it will be useful,
"        but WITHOUT ANY WARRANTY; without even the implied warranty of
"        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"        GNU General Public License for more details.
"
"        You should have received a copy of the GNU General Public License
"        along with this program.  If not, see <https://www.gnu.org/licenses/>.

"----------------------------------------------------------------
" Smart Mark
" A plugin to show a buffers marks in the gutter
"
" Maintainer: Jacob Rahme <jacob@jrahme.ca>
" Version: 0.0.1-alpha
"----------------------------------------------------------------

if !exists('g:smartmark_auto_refresh')
	let g:smartmark_auto_refresh = 1
endif

if !exists('g:smartmark_local_marks')
	let g:smartmark_local_marks = "abcdefghijklmnopqrstuvwxyz.[]<>"
endif

if !exists('g:smartmark_file_marks') 
	let g:smartmark_file_marks = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
endif

if !exists('g:smartmark_refresh_rate')
	let g:smartmark_refresh_rate = 90
endif

let g:smartmark_all_marks = g:smartmark_local_marks . g:smartmark_file_marks

command! SmartMarkAutoToggle call smart_mark#core#auto_refresh_toggle()
command! SmartMarkRefresh call smart_mark#core#refresh_marks('1')
command! SmartMarkReloadMarks	call smart_mark#core#load_marks()

call smart_mark#core#init() 	
