" functions for use when writing functional code
" All functions treat incoming parameters immutably


"immutable collection manipulation{{{{

" -------------------------------------------------------------------
" Name: smart_mark#functional_reverse
" Params: 
"   l: list to reverse
" Desc: Reverse a list without affecting the source
" Return: the reveresed list (list)
" -------------------------------------------------------------------
function! smart_mark#functional#reverse(l) abort
	let l:new_list = deepcopy(a:l)
	call reverse(l:new_list)
	return l:new_list
endfunction
"load this fn for local usage
let s:FN_reverse = function('smart_mark#functional#reverse')

" -------------------------------------------------------------------
" Name: smart_mark#functional#append
" Params: 
"   l: the list to append to
"   val: the value to append to the list
" Desc: Load the marks to display from the global vars
" Return: A list with the new value appended (list)
" -------------------------------------------------------------------
function! smart_mark#functional#append(l, val) abort
	let l:new_list = deepcopy(a:l))
	call add(l:new_list, deepcopy(a:val))
	return l:new_list
endfunction
let s:FN_append = function('smart_mark#functional#append')

" -------------------------------------------------------------------
" Name: smart_mark#functional#remove
" Params: 
"   l: the list to remove from
"   val: The value to remove
" Desc: Remove a value from a list
" Return: The list with the value removed (list)
" -------------------------------------------------------------------
function! smart_mark#functional#remove(l, val) abort
	let l:new_list = deepcopy(a:l)
	let l:idx = index(l:new_list, a:val)
	if l:idx > -1
		call remove(l:new_list, l:idx)
	endif
	return l:new_list
endfunction

" -------------------------------------------------------------------
" Name: smart_mark#functional#assoc
" Params: 
"   l: The collection to associate to
"   index: the index to add the new value to
"   val: the value to append to the collection
" Desc: Add a value to a collection
" Return: The collection with the new mapping (list, dictionary)
" -------------------------------------------------------------------
function! smart_mark#functional#assoc(l, i, val) abort
	let l:new_coll = deepcopy(a:l)
	let l:new_coll[a:i] = deepcopy(a:val)
	return l:new_coll
endfunction
let s:FN_assoc = function('smart_mark#functional#assoc')

" -------------------------------------------------------------------
" Name: smart_mark#functional#pop
" Params: 
"   l: the list to append to
" Desc: Remove a value from a list
" Return: The item removed from the list, the new list (?, list)
" -------------------------------------------------------------------
function! smart_mark#functional#pop(l) abort
	let l:new_list = deepcopy(a:l)
	let l:Item = l:new_list[0]
	call remove(l:new_list, 0)
	return [l:Item, l:new_list]
endfunction
let s:FN_pop = function('smart_mark#functional#pop')

" -------------------------------------------------------------------
" Name: smart_mark#functional#immutable_copy
" Params: 
"   obj: The object to get a copy of
" Desc: Return an immutable version of the object
" Return: An immutable copy of the object (?)
" -------------------------------------------------------------------
function! smart_mark#functional#immutable_copy(obj) abort
	if type(a:obj) == type([]) || type(a:obj) == type({})
		return deepcopy(a:obj)
	else
		let l:obj = a:obj
		return l:obj
endfunction
let s:FN_immutable_copy = function('smart_mark#functional#immutable_copy')
"}}}}

"transformation{{{{

" -------------------------------------------------------------------
" Name: smart_mark#functional#append
" Params: 
"   fn: The function to use for redudcing
"   data: The collection to reduce
"   ...: An empty list or dictionary for the result
" Desc: Apply a functon to the first item in a collection
"				and the initial struct
"				Then take the result of that function call
"				and apply the same function to the result and the
"				next item in the data list.
" Return: The resulting collection from reducing the data (?)
" -------------------------------------------------------------------
function! smart_mark#functional#reduce(fn, data, ...) abort
	if type(a:fn) == 2
		let F = a:fn
	elseif type(a:fn) == 1
		let F = function(a:fn)
	endif
	let initial_struct = get(a:, 1, 0)

	if type(initial_struct) == 0
		let acc = a:data[0] 
		let vals = a:data[1:]
	else
		let acc = initial_struct
		let vals = a:data
	endif

	for value in vals
		let acc = F(acc, value)
	endfor
	return acc
endfunction
"}}}}

"Functional Execution{{{{
function! smart_mark#functional#chain_fn(val, fn_list) abort
" -------------------------------------------------------------------
" Name: smart_mark#functional#chain_fn
" Params: 
"   val: The first value to call with
"   fn_list: A list of function references
" Desc: Take a list of functions and call in order with the value
"       returned from the previous items call, starting with a 
"       given value
" Return: The result of all of the chained function calls
" -------------------------------------------------------------------
	let l:val = s:FN_immutable_copy(a:val)
	if len(a:fn_list) < 1
		return l:val 
	endif
	let [l:Fn_ref, l:fn_list] = s:FN_pop(a:fn_list)
	return smart_mark#functional#chain_fn(l:Fn_ref(l:val), l:fn_list)
endfunction
"}}}}

"loader functions{{{{
" -------------------------------------------------------------------
" Name: smart_mark#functional#get_fn_ref
" Params: 
"   fn_name: The name of a function
" Desc: return a reference to a given function
" Return: A function reference to the named function (function ref)
" -------------------------------------------------------------------
function! smart_mark#functional#get_fn_ref(fn_name)
	return funcref(a:fn_name)
endfunction
"}}}}
