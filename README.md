     ____                       _   __  __            _ 
    / ___| _ __ ___   __ _ _ __| |_|  \/  | __ _ _ __| | __ 
    \___ \| '_ ` _ \ / _` | '__| __| |\/| |/ _` | '__| |/ / 
     ___) | | | | | | (_| | |  | |_| |  | | (_| | |  |   < 
    |____/|_| |_| |_|\__,_|_|   \__|_|  |_|\__,_|_|  |_|\_\ 

# Smart Mark

A plugin to show marks in the gutter of a buffer.

Smart Mark utilizes the timers feature of vim to automatically update the listed marks,
as well was designed in a functional manner, with an attempt to not rely on the given 
state of any data structures for the set of active marks.

## Requirements

Vim versoin 8.0 or higher, compiled with the +timers feature. To check if your installed verison of Vim
meets these requiresments run :version in a Vim session.

##	Installation, Configuration, and Otherthings

Please see the doc/smartmark.txt help file for details on installation and configuration

## Why?

Several other plugins do exist for displaying the marks, but none of them worked out of the
box for me, and I wanted a plugin that just worked without any configuration required on my end. 

As well I was looking for an excuse to try my hand at writing a plugin and to play with asynchronous
features added in Vim8. 

## Contributing

Issues, and Pull Requests are always more than welcome, and I'll do my best to keep up with any that pop up
but this project is only done as a hobby, so no promises on turn around time. 

Pull Requets should be made against the development branch.

## Supporting

You can support Smart Mark in several ways

### Contribute

Pull requests, issues, and code review all help the project grow, and gain direction. 

### Distribute
Star the project, tell your firends, colleagues, ex lovers and frenemies about it! 

### Donate
Donations are always gladly accepted, and the more financial support a project has the more of my time I'm able
to dedicate to supporting it! 


###### Bitcoin
37tgs4NZFvmPCnimPWmmCopRoftPaNgfFE

###### Monero
44uKvFQ5sFkaoeaBA5duQk5xMoZqxiYscGySamJQK2saEXbLn6b8pa3Yt3E1cHeBDmZfR3vBwa37uNZgUao4z9Gi8aQNN4m

###### Ethereum
0x160362bdF63604803C29f2dD3533E118C483c9a6

## License 
[GPL v3](http://www.gnu.org/licenses/gpl-3.0.html)
