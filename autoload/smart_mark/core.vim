
"smart mark control API{{{{

" ----------------------------------------------------------------------------
" Name: smart_mark#core#auto_refresh_toggle
" Params: None
" Desc: Toggle auto refreshing marks of and on
" Return: None
" ----------------------------------------------------------------------------
function! smart_mark#core#auto_refresh_toggle() abort
	if !exists('s:mark_timer')
		call s:auto_refresh()
	else
		call s:stop_auto_refresh()
	endif
endfunction

" ----------------------------------------------------------------------------
" Name: smart_mark#core#refresh_marks
" Params: None
" Desc: Evaluate placed marks, display in gutter
" Return: None
" ----------------------------------------------------------------------------
function! smart_mark#core#refresh_marks(...) abort
	if g:smartmark_auto_refresh == 0
		call timer_stop(s:mark_timer)
		return
	endif
	"data for active marks, and currently placed smart_mark signs
	"{mark_char : [index, line, name]}
	let l:active_mark_data = smart_mark#functional#reduce(function('s:get_visible_marks'), g:smartmark_all_marks, {})
	"data for currently placed signs
	"{sign_text : line_number}
	let l:placed_smart_mark_signs = s:smart_mark_placed_signs()
	"new and moved marks should be able to be done over 1 iteration of the
	"placed_smart_mark_signs instead of 2
	"signs to be placed
	let l:F_new_marks = {id, mark -> has_key(l:placed_smart_mark_signs, mark) == 0}
	let l:new_marks = filter(keys(active_mark_data), l:F_new_marks)
	"signs to be moved
	let l:F_moved_marks = {id, mark -> has_key(l:placed_smart_mark_signs, mark) && l:placed_smart_mark_signs[mark] != l:active_mark_data[mark][1]}
	let l:moved_marks = filter(keys(active_mark_data), l:F_moved_marks)
	"signs to be removed
	let l:F_removed_marks = {id, sign -> has_key(l:active_mark_data, sign) == 0}
	let l:removed_marks = filter(keys(l:placed_smart_mark_signs), l:F_removed_marks)

	for mrk in l:new_marks
		let l:mrk_data = l:active_mark_data[mrk]
		call s:place_mark(l:mrk_data[0], l:mrk_data[1], l:mrk_data[2])
	endfor

	for mrk in l:moved_marks
		let l:mrk_data = l:active_mark_data[mrk]
		call s:remove_mark(mrk)
		call s:place_mark(l:mrk_data[0], l:mrk_data[1], l:mrk_data[2])
	endfor

	for mrk in l:removed_marks
		call s:remove_mark(mrk)
	endfor

	for mrk in keys(l:active_mark_data)
		let l:mrk_data = l:active_mark_data[mrk]
		call s:place_mark(l:mrk_data[0], l:mrk_data[1], l:mrk_data[2])
	endfor
endfunction

" ----------------------------------------------------------------------------
" Name: smart_mark#core#init
" Params: None
" Desc: initialize mark signs and start refresh
" Return: None
" ----------------------------------------------------------------------------
function! smart_mark#core#init()
	call smart_mark#core#load_marks()
	for mc in g:smartmark_all_marks
		let l:mark_name = s:get_mark_name(mc)
		execute(printf('sign define %s text=%s', l:mark_name, mc))
	endfor
	
	if has('timers') && g:smartmark_auto_refresh == 1
		call s:auto_refresh()
	endif
endfunction

" ----------------------------------------------------------------------------
" Name: s:auto_refresh
" Params: None
" Desc: start automatically refreshing mark signs
" Return: None
" ----------------------------------------------------------------------------
function! s:auto_refresh() abort
	if !exists('s:mark_timer')
		let s:mark_timer = timer_start(g:smartmark_refresh_rate, 'smart_mark#core#refresh_marks', {'repeat' : -1})
	endif
endfunction

" ----------------------------------------------------------------------------
" Name: s:stop_auto_refresh
" Params: None
" Desc: Stop automatically refreshing mark signs
" Return: None
" ----------------------------------------------------------------------------
function! s:stop_auto_refresh() abort
	if exists('s:mark_timer')
		call timer_stop(s:mark_timer)
		unlet s:mark_timer
		call s:remove_all_marks()
	endif
endfunction
"}}}}

"smart mark functions{{{{

" ----------------------------------------------------------------------------
" Name: smart_mark#core#load_marks
" Params: None
" Desc: Load the marks to display from the global vars
" Return: None
" ----------------------------------------------------------------------------
function! smart_mark#core#load_marks() abort
	let g:smartmark_local_marks = split(g:smartmark_local_marks, '\zs')
	let g:smartmark_file_marks = split(g:smartmark_file_marks, '\zs')
	let g:smartmark_all_marks = split(g:smartmark_all_marks, '\zs')
endfunction                    	

" ----------------------------------------------------------------------------
" Name: s:get_visible_marks
" Params: 
"   coll: map of mark information lists
"   character: The character to add to the collection
" Desc: Load the marks to display from the global vars
" Return: coll, with the data for a:character associated (dictionary)
" ----------------------------------------------------------------------------
function! s:get_visible_marks(coll, character)
	let l:mark_line = line("'".a:character)
	if l:mark_line > 0 && s:mark_is_for_buffer(a:character)
		let l:mark_index = s:get_mark_id(a:character)
		let l:mark_name = s:get_mark_name(a:character)
		return smart_mark#functional#assoc(a:coll, a:character, [l:mark_index, l:mark_line, l:mark_name])
	endif
	return deepcopy(a:coll)
endfunction

" ----------------------------------------------------------------------------
" Name: s:mark_is_for_buffer
" Params: 
"   mark: The mark character to check
" Desc: Check if a specific mark charater is for current buf
" Return: boolean (0, 1)
" ----------------------------------------------------------------------------
function! s:mark_is_for_buffer(mark) abort
	if index(g:smartmark_local_marks, a:mark) > -1
		return 1
	else 
		return getpos("'" . a:mark)[0] == bufnr('%')
	endif
endfunction

" ----------------------------------------------------------------------------
" Name: s:place_mark
" Params: 
"   index: the index of the sign for the mark to place
"   line: the line to place the sign on
"   name: the name of the sign to place
" Desc: Place a mark in the gutter 
" Return: None
" ----------------------------------------------------------------------------
function! s:place_mark(index, line, name) abort
	execute(printf('sign place %s line=%s name=%s buffer=%s', a:index, a:line, a:name, bufnr('%')))
endfunction

" ----------------------------------------------------------------------------
" Name: s:remove_mark
" Params: 
"   character: the mark character to remove the sign for
" Desc: Removes a sign from the gutter for the given mark
" Return: None
" ----------------------------------------------------------------------------
function! s:remove_mark(character) abort
	execute(printf('sign unplace %s buffer=%s',s:get_mark_id(a:character),bufnr('%')))
endfunction

" ----------------------------------------------------------------------------
" Name: s:remove_all_marks
" Params: None
" Desc: Remove all marks placed by smart mark in the buffer
" Return: None
" ----------------------------------------------------------------------------
function! s:remove_all_marks()
	for mrk in keys(s:smart_mark_placed_signs())
		call s:remove_mark(mrk)
	endfor
endfunction
"}}}}

"smart mark utilities{{{{

" ----------------------------------------------------------------------------
" Name: s:get_signs_for_line
" Params: 
"   ln: the line number to get the signs for
" Desc: Get a collection of signs for a specific line
" Return: A human readable list of signs for a specific line (list)
" ----------------------------------------------------------------------------
function! s:get_signs_for_line(ln) abort
	"append collection with sign_id if it is on the given line
	let l:T_line_signs = {coll, sign_id -> coll + matchlist(sign_id, printf('line=\(%s\).*name=\(\p*\)', a:ln))[2:2]}
	"translate sign_id to human readable character
	let l:T_sign_to_text = {coll, sign_id -> coll + split(split(execute(printf('sign list %s', sign_id)))[2], '=')[1:1]}
	let signs = split(execute(printf('silent! sign place buffer=%s',bufnr('%'))), '\n')[2:]
	return smart_mark#functional#reduce(l:T_sign_to_text, smart_mark#functional#reduce(s:T_line_signs, signs, []), [])
endfunction

" ----------------------------------------------------------------------------
" Name: s:smart_mark_placed_signs
" Params: None
" Desc: Get a coll of signs placed by smart_mark in the buffer
" Return: A dictionary of signs to their placement data (dictionary)
" ----------------------------------------------------------------------------
function! s:smart_mark_placed_signs() abort
	"returns a list of tuples of the line number and sign text
	let l:F_smart_mark_filter = {key, val -> match(val, 'smart_mark') > -1}
	let l:T_sign_to_text = {coll, sign_str -> add(coll, matchlist(sign_str, 'line=\(\d*\).*name=smart_mark_\(\p*\)')[1:2])}
	let l:T_tuple_to_dict = {coll, sign_tuple -> smart_mark#functional#assoc(coll, sign_tuple[1], sign_tuple[0])}

	let l:signs = split(execute(printf('silent! sign place buffer=%s',bufnr('%'))), '\n')[2:]
	let l:smart_mark_signs = filter(l:signs, l:F_smart_mark_filter)
	let l:sign_tuples = smart_mark#functional#reduce(l:T_sign_to_text,l:signs, [])
	return smart_mark#functional#reduce(l:T_tuple_to_dict, l:sign_tuples, {})
endfunction

" ----------------------------------------------------------------------------
" Name: s:get_mark_id
" Params:
"   mark_character: the character to get the id for
" Desc: Generate the id of a sign for a smart mark character
" Return: The a:mark_character sign id (string)
" ----------------------------------------------------------------------------
function! s:get_mark_id(mark_character) abort
	return index(g:smartmark_all_marks, a:mark_character) + 1
endfunction

" ----------------------------------------------------------------------------
" Name: s:get_mark_name
" Params:
"   mark_character: the mark character to get the sign name for
" Desc: Get the name of the sign for a mark
" Return: the sign name for the character (String)
" ----------------------------------------------------------------------------
function! s:get_mark_name(mark_character) abort
	return 'smart_mark_'.a:mark_character
endfunction

" ----------------------------------------------------------------------------
" Name: s:mark_is_active
" Params:
"   mark_character: The mark to check
" Desc: Check if a mark is currently set
" Return: boolean (0,1)
" ----------------------------------------------------------------------------
function! s:mark_is_active(mark_character) abort
	if line("'".a:mark_character) > -1
		return 1
	else
		return 0
	endif
endfunction
"}}}}

